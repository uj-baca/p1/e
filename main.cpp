#include <iostream>

#include <string>

using namespace std;

string NormalizujNapis(string base);

string FormatujNapis(string base, const string &arg0, const string &arg1, const string &arg2);

string UsunSlowo(string base, int number);

string NajwiekszeSlowo(string base);


string FormatujNapis(string base, const string &arg0, const string &arg1, const string &arg2) {
    string cmd;
    string result;
    bool cmdFlag = false;
    char what;
    int tmp, id;
    for (int i = 0; base[i] != '\0'; i++) {
        if (base[i] == '{') {
            cmdFlag = true;
            continue;
        }
        if (base[i] == '}') {
            what = cmd[0];
            if (what == 'p') {
                if (cmd[2] < '0' || cmd[2] > '9') {
                    continue;
                }
                id = cmd[2] - '0';
                for (int j = 0; j < id; j++) {
                    result += cmd[4];
                }
            } else if (what == 'u') {
                if (cmd[2] < '0' || cmd[2] > '9') {
                    continue;
                }
                id = cmd[2] - '0';
                i += id;
            } else if (what == 'U') {
                if (cmd[2] < '0' || cmd[2] > '9') {
                    continue;
                }
                id = cmd[2] - '0';
                result.resize(result.size() - id);
            } else if (what == 'w') {
                if (cmd[2] < '1' || cmd[2] > '3') {
                    continue;
                }
                id = cmd[2] - '0';
                if (id == 1) {
                    result += arg0;
                } else if (id == 2) {
                    result += arg1;
                } else if (id == 3) {
                    result += arg2;
                }
            } else if (what == 'W') {
                if (cmd[2] < '1' || cmd[2] > '3') {
                    continue;
                }
                id = cmd[2] - '0';
                if (cmd[4] < '0' || cmd[4] > '9') {
                    continue;
                }
                tmp = cmd[4] - '0';
                string selected;
                if (id == 1) {
                    selected = arg0;
                } else if (id == 2) {
                    selected = arg1;
                } else if (id == 3) {
                    selected = arg2;
                }
                selected.resize(tmp);
                for (int j = 0; j < selected.size(); j++) {
                    if (selected[j] == 0) {
                        selected[j] = ' ';
                    }
                }
                result += selected;
            }
            cmd = "";
            cmdFlag = false;
            continue;
        }
        if (cmdFlag) {
            cmd += base[i];
        } else {
            result += base[i];
        }
    }
    return result;
}

string UsunSlowo(string base, int number) {
    int word = 1;
    string result;
    bool ignore = true;
    for (int i = 0; base[i] != '\0'; i++) {
        if (base[i] != ' ') {
            ignore = false;
        }
        if (word != number || base[i] == ' ') {
            result += base[i];
        }
        if (ignore) {
            continue;
        }
        if (base[i] == ' ' && base[i - 1] != ' ') {
            word++;
        }
    }
    return result;
}

string NajwiekszeSlowo(string base) {
    int count = 0;
    for (int i = 0; base[i] != '\0'; i++) {
        if (base[i] == ' ') {
            count++;
        }
    }
    string words[count + 1];
    count = 0;
    for (int i = 0; i <= count; i++) {
        words[i] = "";
    }
    for (int i = 0; base[i] != '\0'; i++) {
        if (base[i] == ' ') {
            count++;
        } else {
            words[count] += base[i];
        }
    }
    string biggest = words[0];
    for (int i = 1; i <= count; i++) {
        if (words[i] > biggest) {
            biggest = words[i];
        }
    }
    return biggest;
}

string NormalizujNapis(string base) {
    string result;
    int lastIndex = -1;
    bool space = false;
    for (int i = 0; base[i] != '\0'; i++) {
        if (base[i] == ' ') {
            space = true;
        } else {
            if (space) {
                result += ' ';
                space = false;
            }
            result += base[i];
        }
    }
    base = result;
    result = "";
    for (int i = 0; base[i] != '\0'; i++) {
        if (lastIndex < 0 && base[i] == ' ') {
            continue;
        } else {
            if (base[i] == '.' || base[i] == ',') {
                result += base[i];
                if (base[i + 1] != ' ') {
                    result += ' ';
                }
            } else if (base[i] == ' ') {
                if (base[i + 1] == '.' || base[i + 1] == ',') {
                    result += base[i + 1];
                    i++;
                    if (base[i + 2] != ' ') {
                        result += ' ';
                    }
                } else {
                    result += base[i];
                }
            } else {
                result += base[i];
            }
            lastIndex++;
        }
    }
    base = result;
    result = "";
    space = false;
    for (int i = 0; base[i] != '\0'; i++) {
        if (base[i] == ' ') {
            space = true;
        } else {
            if (space) {
                result += ' ';
                space = false;
            }
            result += base[i];
        }
    }
    while (result[result.size() - 2] == ' ') {
        result.resize(result.size() - 2);
    }
    return result;
}

int main() {
    return 0;
}
